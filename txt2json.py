"""

imports chat logs in txt format and spews out a json format that our server can understand

note: code works most of the time
      it breaks when there is a msg line that starts and ends with brackets, example "(this line causes issues)"
      because we assume a beginning chat line format like "(datetime) name: msg"
"""
import os
import uuid
import json
import time
import logging
import datetime

NAME_COMPANY = "Upwork"
NAME_AGENT = "Agent"
NAME_VISITOR = "Visitor"
CHANNEL_HEADER = "#supportchat:"

HEADERS = ['Timestamp:','Unread:','Visitor ID:','Visitor Name:','Visitor Email:','Visitor Notes:','IP:','Country Code:',\
           'Country Name:','Region:','City:','User Agent:','Platform:','Browser:']

chatscounter = 0

def getfiles():
    logfiles = [f for f in os.listdir() if "2017-11" in f and ".text" in f]
    # logging.info(logfiles)
    return logfiles


def isCode(line):
    clean = line.strip()
    status = False
    for h in HEADERS :
        if clean.startswith(h) :
            status = True
    return status


def changetimeformat(oldformat) :
    dt = None
    rt = None
    clean = oldformat.strip()
    if 'T' in clean and 'Z' in clean :
        try :
            dt = time.strptime(clean, "%Y-%m-%dT%H:%M:%SZ")
            rt = str(time.strftime("%Y-%m-%dT%H:%M:%S.000000Z", dt))
        except Exception :
            rt = None
    else :
        try :
            dt = time.strptime(clean, "%Y-%m-%d %H:%M:%S")
            rt = str(time.strftime("%Y-%m-%dT%H:%M:%S.000000Z", dt))
        except Exception :
            rt = None
    return rt

    
def processfile(f, chats):
    global chatscounter
    
    visitor = NAME_VISITOR
    agent = NAME_AGENT
    type = "chat.msg"
    channel = CHANNEL_HEADER+str(uuid.uuid1().hex)
    ts = ""
    item = None
    #
    data = open(f, "r")
    if data is not None :
        history = []

        item = {}
        item['name'] = NAME_AGENT
        item['type'] = "chat.memberjoin"
        history.append(item)
        item = {}
        item['name'] = NAME_VISITOR
        item['type'] = "chat.memberjoin"
        history.append(item)
        item = None

        for line in data :
            try :
                # logging.info(line)
                #
                if line is not None and line.startswith("==============================") :
                    # store prev chatlog, if any
                    # logging.info(line)
                    if item is not None :
                        history.append(item)
                    chat = {}
                    chat['type'] = "chat"
                    chat['unread'] = False
                    chat['agent'] = agent
                    chat['visitor'] = visitor
                    chat['history'] = history
                    
                    chatscounter = chatscounter+1
                    chatname = '{0:05d}'.format(chatscounter) + '-' + uuid.uuid1().hex
                    chats[chatname] = chat
                    
                    # initialize new chatlog
                    history = []

                    item = {}
                    item['name'] = NAME_AGENT
                    item['type'] = "chat.memberjoin"
                    history.append(item)
                    item = {}
                    item['name'] = NAME_VISITOR
                    item['type'] = "chat.memberjoin"
                    history.append(item)
                    item = None

                    item = None
                elif line is not None and line.startswith("Timestamp:") :
                    ts = changetimeformat(line[len("Timestamp: "):])
                elif line is not None and line.startswith("Visitor ID:") :
                    channel = CHANNEL_HEADER+line[len("Visitor ID: "):].strip()
                elif line is not None and line.startswith("Visitor Name:") :
                    visitor = line[len("Visitor Name: "):].strip()
                elif line is not None and line.startswith("(") :
                    # is this another chatline or beginning of a new turn
                    ts = None
                    start = 0
                    end = line.index(")")
                    if start < end :
                        ts = changetimeformat(line[start+1:end])
                    if ts is not None :
                        # save existing chat
                        if item is not None and 'msg' in item :
                            history.append(item)
                            item = None
                        #
                        start = 0
                        end = line.index(")")
                        if start < end :
                            ts = changetimeformat(line[start+1:end])
                            start = end+2
                            end = line.index(":", start)
                            if start < end :
                                name = line[start:end].strip()
                                msg = line[end+2:]
                                if name is not None and NAME_COMPANY not in name :
                                    # logging.info("visitor: "+visitor+", name: "+name)
                                    if visitor in name :
                                        person = NAME_VISITOR
                                        realname = name
                                        visitor = name
                                    else :
                                        person = NAME_AGENT
                                        realname = name
                                        agent = name
                                    item = {}
                                    item['timestamp'] = ts
                                    item['channel'] = channel
                                    item['type'] = type
                                    item['name'] = person
                                    item['msg'] = msg
                                    item['realname'] = name
                                    # logging.info(item)
                            else :
                                logging.warning("Cannot parse name from chatline: "+line)
                        else :
                            logging.warning("Cannot parse timestamp from chatline: "+line)
                    else :
                        # its another chatline
                        if item is not None and 'msg' in item:
                            item['msg'] = item['msg'] + line
                        else :
                            logging.warning("No item to store chatline")
                elif line is not None and len(line)>1 and not isCode(line) :
                    if item is not None and 'msg' in item:
                        item['msg'] = item['msg'] + line
                    else :
                        logging.warning("No item to store chatline: "+line)
                    #else :
                    #    logging.warning("Unknown status:")
                    #    logging.warning(line)
                # else :
                #    # nothing to do here
                #    logging.info("isCode: "+str(isCode(line)))
            except Exception as e:
                logging.error("Exception: "+str(e))
                logging.error(f)
                logging.error(line)
                

def savechats(data):
    f = open("txt2json_output.json", "w")
    f.write(data)
    f.close()        


if __name__ == "__main__":
    #
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
    logging.info('starting...')
    logging.info('processing chatlogs for '+NAME_COMPANY)
    
    chats = {}
    
    filelist = getfiles()
    
    # for testing w a testfile

    """
    #processfile("2017-00-00.test", chats)
    processfile("2017-00-01.test", chats)
    #logging.info(json.dumps(chats))
    savechats(json.dumps(chats))
    """

    for f in filelist[0:16] :
        logging.info(f)
        processfile(f, chats)
    
    savechats(json.dumps(chats))
    
    logging.info("done!")
    
    
    
